using Turing, Distributions
using RDatasets

# Plots and StatsPlots for data visualization and diagnostics
using Plots, StatsPlots
pyplot()

# Functionality for splitting and normalizing the data.
using MLDataUtils: shuffleobs, splitobs, rescale!

# Functionality for evaluating the model predictions.
using Distances

# Set a seed for reproducibility.
using Random
Random.seed!(0)

using LinearAlgebra

# Import the "Default" dataset.
data = RDatasets.dataset("datasets", "mtcars");

# Show the first six rows of the dataset.
first(data, 6)

# Remove the model column.
select!(data, Not(:Model))

# Split our dataset 70%/30% into training/test sets.
trainset, testset = splitobs(shuffleobs(data), 0.7)

# Turing requires data in matrix form.
target = :MPG
train = Matrix(select(trainset, Not(target)))
test = Matrix(select(testset, Not(target)))
train_target = trainset[:, target]
test_target = testset[:, target]

# Standardize the features.
μ, σ = rescale!(train; obsdim = 1)
rescale!(test, μ, σ; obsdim = 1)

# Standardize the targets.
μtarget, σtarget = rescale!(train_target; obsdim = 1)
rescale!(test_target, μtarget, σtarget; obsdim = 1);


# Implementation of Bishop's Chapter 3

# α = 1/3.0
# β = 1/1e5

N = size(train, 1)  # This is the number of data points
Φ = hcat(ones(N), train)
M = size(Φ, 2)  # This is the number of basis functions in Φ
t = train_target

R = (Φ' * Φ)
λᵣ = eigvals(R)

Sinv = Array{Matrix{Float64}, 1}()
m = Array{Array{Float64}, 1}()
α = Float64[]
β = Float64[]
push!(α, 1e-11)
push!(β, 1e-11)

for j = 1:10
    λ = β[j]*λᵣ
    # Bayesian Linear Regression
    push!(Sinv, α[j]*I + β[j]*R)
    push!(m, β[j]*(Sinv[j]\(Φ'))*t)

    # Evidence maximization
    γ = sum( λ[i] / (α[j] + λ[i]) for i = 1:M )
    push!(α, γ / dot(m[j], m[j]))
    total_sse = sum( t - Φ*m[j] ) do x
        x^2
    end # This is the same as: sum(abs2, t - Φ*m)
    push!( β, (N - γ) * 1 / total_sse )
end
S = Matrix(Hermitian(inv(Sinv[end])))

for i = 1:7
    w = MvNormal(m[i], S)

    # # Visualize the posterior on w
    # p = plot(Normal(w.μ[1], w.Σ[1,1]))
    # for i = 2:11
    #     plot!(p, Normal(w.μ[i], w.Σ[i,i]))
    # end
    # display(p)

    X = range(-1, 1, length=100)
    Y = range(-1, 1, length=100)
    i = 9; j = 11
    d = MvNormal(w.μ[[i,j]], w.Σ[[i,j],[i,j]])
    f(x,y) = pdf(d, [x,y])
    p = contourf(X, Y, f, color=:viridis)
    display(p)
    sleep(0.5)
end

# Compute the predictive distribution
# i = rand(1:10, 1)[1]
# # N = size(test, 1)  # This is the number of data points
# ϕ = hcat(1, test[i,:]...)[1,:]
# μ = dot(ϕ, m[end])
# σ² = 1/β[end] + dot(ϕ, S, ϕ)

# plot(Normal(μ, √σ²))

μ = Float64[]
σ_sq = Float64[]
for j = 1:length(test_target)
    ϕ = hcat(1, test[j,:]...)[1,:]
    push!(μ, dot(ϕ, m[end]))
    push!(σ_sq, 1/β[end] + dot(ϕ, S, ϕ))
end

hcat(μ, test_target) |> display
# @info sum(abs2, μ - test_target)